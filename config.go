package cv

import (
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/mitchellh/go-homedir"
	"github.com/pelletier/go-toml"
	"github.com/pkg/errors"
)

type Config interface {
	SetDefaults() error
	LogOut() io.Writer
}

func printPath(path string) string {
	full, err := filepath.Abs(path)
	if err != nil {
		return fmt.Sprintf("无法解读文件名：%v %v", path, err)
	}
	return full
}

func ReadConfigFile(path string, config Config) (bool, error) {
	f, err := os.Open(path)
	if err != nil {
		if !os.IsNotExist(err) {
			return false, errors.WithMessagef(err, "不能打开设置文件 %v", printPath(path))
		}
		fmt.Fprintln(config.LogOut(), "使用桌面设置文件")
		hd, err := homedir.Dir()
		if err != nil {
			return false, errors.WithMessage(err, "unknow homedir.Dir()")
		}
		path = hd + `/桌面/` + path
		f, err = os.Open(path)
		if err != nil {
			return false, errors.WithMessagef(err, "不能打开设置文件 %v", printPath(path))
		}
	}
	defer f.Close()
	dec := toml.NewDecoder(f)
	err = dec.Decode(config)
	if err != nil {
		return false, errors.WithMessagef(err, "设置文件(%v)读取错误", path)
	}
	return true, nil
}

func WriteConfigFile(path string, config Config) (bool, error) {
	f, err := os.Open(path)
	if err != nil {
		if !os.IsNotExist(err) {
			return false, errors.WithMessagef(err, "不能打开设置文件 %v", printPath(path))
		}
		fmt.Fprintln(config.LogOut(), "使用桌面设置文件")
		hd, err := homedir.Dir()
		if err != nil {
			return false, errors.WithMessage(err, "unknow homedir.Dir()")
		}
		path = hd + `/桌面/` + path
		f, err = os.Open(path)
		if err != nil {
			return false, errors.WithMessagef(err, "不能打开设置文件 %v", printPath(path))
		}
	}
	defer f.Close()
	dec := toml.NewDecoder(f)
	err = dec.Decode(config)
	if err != nil {
		return false, errors.WithMessagef(err, "设置文件(%v)读取错误", path)
	}
	return true, nil
}
