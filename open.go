package cv

import (
	"sync"

	"github.com/pkg/errors"
	"go.uber.org/multierr"
	"gocv.io/x/gocv"
)

var opened = make(map[int]*VideoDecoder)
var openedLock = sync.Mutex{}

type VideoDecoder struct {
	*gocv.VideoCapture
	DeviceID          int
	Slot              int
	ReadErrorCount    int
	DecoderErrorCount int
}

func OpenedDecoderCount() int {
	openedLock.Lock()
	defer openedLock.Unlock()
	return len(opened)
}

func OpenNextVideoDecoder(maxID int) (*VideoDecoder, error) {
	openedLock.Lock()
	defer openedLock.Unlock()
	var errs error
	for i := 0; i <= maxID; i++ {
		if _, ok := opened[i]; ok {
			continue
		}
		var webcam *gocv.VideoCapture
		var err error
		Do(func() {
			webcam, err = gocv.VideoCaptureDevice(i)
		})
		if err != nil {
			errs = multierr.Append(errs, err)
			continue
		}
		slot := 0
		if i > 1 {
			slot = 1
		}
		for _, o := range opened {
			if o.Slot == slot {
				slot = (slot + 1) % 2
			}
		}
		opened[i] = &VideoDecoder{
			VideoCapture: webcam,
			Slot:         slot,
			DeviceID:     i,
		}
		return opened[i], nil
	}
	if errs == nil {
		return nil, errors.Errorf("all cameras within maxID %v are already opened", maxID)
	}
	return nil, errors.WithMessage(errs, "can not open any camers")
}

func (v *VideoDecoder) Close() (err error) {
	Do(func() {
		err = v.VideoCapture.Close()
	})
	openedLock.Lock()
	defer openedLock.Unlock()
	delete(opened, v.DeviceID)
	return err
}
