// Arrange that main.main runs on main thread.
//    func init() {
//	    runtime.LockOSThread()
//    }
package cv

import (
	"context"
)

// queue of work to run in main thread.
var mainfunc = make(chan func())

// Run must start from main thread
func Run(ctx context.Context) error {
	for {
		select {
		case f := <-mainfunc:
			f()
		case <-ctx.Done():
			return ctx.Err()
		}
	}
}

// do runs f on the main thread.
func Do(f func()) {
	done := make(chan bool, 1)
	mainfunc <- func() {
		f()
		done <- true
	}
	<-done
}
