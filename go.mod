module bitbucket.org/xiegeo/cv

go 1.14

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pelletier/go-toml v1.8.1
	github.com/pkg/errors v0.9.1
	go.uber.org/multierr v1.6.0
	gocv.io/x/gocv v0.22.0
)
